#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Taken and customed from Jack Valmadre's Blog:
# http://jackvalmadre.wordpress.com/2008/09/21/resizable-image-control/
#
# Put together and created the time switching by Izidor Matusov <izidor.matusov@gmail.com>
# Depedencies: python-webkit, avprobe



import os
import pygtk
pygtk.require('2.0')
import gtk
gtk.gdk.threads_init()
import glib
import sys
import vlc
import subprocess
import json




# Create a single vlc.Instance() to be shared by (possible) multiple players.
instance = vlc.Instance()

class ResizableImage(gtk.DrawingArea):

    def __init__(self, aspect=True, enlarge=False,
            interp=gtk.gdk.INTERP_NEAREST, backcolor=None, max=(1600,1200)):
        """Construct a ResizableImage control.

        Parameters:
        aspect -- Maintain aspect ratio?
        enlarge -- Allow image to be scaled up?
        interp -- Method of interpolation to be used.
        backcolor -- Tuple (R, G, B) with values ranging from 0 to 1,
            or None for transparent.
        max -- Max dimensions for internal image (width, height).

        """
        super(ResizableImage, self).__init__()
        self.pixbuf = None
        self.aspect = aspect
        self.enlarge = enlarge
        self.interp = interp
        self.backcolor = backcolor
        self.max = max
        self.connect('expose_event', self.expose)
        self.connect('realize', self.on_realize)

    def on_realize(self, widget):
        if self.backcolor is None:
            color = gtk.gdk.Color()
        else:
            color = gtk.gdk.Color(*self.backcolor)

        self.window.set_background(color)

    def expose(self, widget, event):
        # Load Cairo drawing context.
        self.context = self.window.cairo_create()
        # Set a clip region.
        self.context.rectangle(
            event.area.x, event.area.y,
            event.area.width, event.area.height)
        self.context.clip()
        # Render image.
        self.draw(self.context)
        return False

    def draw(self, context):
        # Get dimensions.
        rect = self.get_allocation()
        x, y = rect.x, rect.y
        # Remove parent offset, if any.
        parent = self.get_parent()
        if parent:
            offset = parent.get_allocation()
            x -= offset.x
            y -= offset.y
        # Fill background color.
        if self.backcolor:
            context.rectangle(x, y, rect.width, rect.height)
            context.set_source_rgb(*self.backcolor)
            context.fill_preserve()
        # Check if there is an image.
        if not self.pixbuf:
            return
        width, height = self.resizeToFit(
            (self.pixbuf.get_width(), self.pixbuf.get_height()),
            (rect.width, rect.height),
            self.aspect,
            self.enlarge)
        x = x + (rect.width - width) / 2
        y = y + (rect.height - height) / 2
        context.set_source_pixbuf(
            self.pixbuf.scale_simple(width, height, self.interp), x, y)
        context.paint()

    def set_from_pixbuf(self, pixbuf):
        width, height = pixbuf.get_width(), pixbuf.get_height()
        # Limit size of internal pixbuf to increase speed.
        if not self.max or (width < self.max[0] and height < self.max[1]):
            self.pixbuf = pixbuf
        else:
            width, height = self.resizeToFit((width, height), self.max)
            self.pixbuf = pixbuf.scale_simple(
                width, height,
                gtk.gdk.INTERP_BILINEAR)
        self.invalidate()

    def set_from_file(self, filename):
        self.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(filename))

    def invalidate(self):
        self.queue_draw()

    def resizeToFit(self, image, frame, aspect=True, enlarge=False):
        """Resizes a rectangle to fit within another.

        Parameters:
        image -- A tuple of the original dimensions (width, height).
        frame -- A tuple of the target dimensions (width, height).
        aspect -- Maintain aspect ratio?
        enlarge -- Allow image to be scaled up?

        """
        if aspect:
            return self.scaleToFit(image, frame, enlarge)
        else:
            return self.stretchToFit(image, frame, enlarge)

    def scaleToFit(self, image, frame, enlarge=False):
        image_width, image_height = image
        frame_width, frame_height = frame
        image_aspect = float(image_width) / image_height
        frame_aspect = float(frame_width) / frame_height
        # Determine maximum width/height (prevent up-scaling).
        if not enlarge:
            max_width = min(frame_width, image_width)
            max_height = min(frame_height, image_height)
        else:
            max_width = frame_width
            max_height = frame_height
        # Frame is wider than image.
        if frame_aspect > image_aspect:
            height = max_height
            width = int(height * image_aspect)
        # Frame is taller than image.
        else:
            width = max_width
            height = int(width / image_aspect)
        return (width, height)

    def stretchToFit(self, image, frame, enlarge=False):
        image_width, image_height = image
        frame_width, frame_height = frame
        # Stop image from being blown up.
        if not enlarge:
            width = min(frame_width, image_width)
            height = min(frame_height, image_height)
        else:
            width = frame_width
            height = frame_height
        return (width, height)

class VLCWidget(gtk.DrawingArea):
    """Simple VLC widget.

    Its player can be controlled through the 'player' attribute, which
    is a vlc.MediaPlayer() instance.
    """
    def __init__(self, *p):
        gtk.DrawingArea.__init__(self)
        self.player = instance.media_player_new()
        def handle_embed(*args):
            if sys.platform == 'win32':
                self.player.set_hwnd(self.window.handle)
            else:
                self.player.set_xwindow(self.window.xid)
            return True
        self.connect("map", handle_embed)
        self.set_size_request(320, 200)

class DemoGtk:

    SLIDE_LABEL = "l"
    SLIDE_PHOTO = "p"
    SLIDE_VIDEO = "v"


    SECONDS_BETWEEN_PICTURES = 3
    FULLSCREEN = False
    color_bg = gtk.gdk.color_parse('#000000')
    color_text = gtk.gdk.color_parse('#7CCCBA')

    slides = []
    index = 0
    photosCount = 0
    moviesLength = 0.0
    timer = 0

    def __init__(self):

        self.directory = sys.argv[1]
        # self.directory = "."

        self.load_image = gtk.Image()
        self.load_image.set_from_animation(gtk.gdk.PixbufAnimation("loading.gif"))

        self.label = gtk.Label(self.get_slideshow_title(self.directory))
        self.label.modify_fg(gtk.STATE_NORMAL, self.color_text)

        self.image = ResizableImage( True, True, gtk.gdk.INTERP_BILINEAR)

        self.vlc_widget = VLCWidget()
        self.vlc_widget.modify_bg(gtk.STATE_NORMAL, self.color_bg)

        self.player = self.vlc_widget.player

        self.manager = self.player.event_manager()
        self.manager.event_attach(vlc.EventType.MediaPlayerEndReached, self.on_video_stop, 1)

        vbox = gtk.VBox(homogeneous=False, spacing=0)
        vbox.pack_start(self.load_image)
        vbox.pack_start(self.label)
        vbox.pack_start(self.image)
        vbox.pack_start(self.vlc_widget)
        vbox.show()

        self.window = gtk.Window()
        self.window.connect('destroy', gtk.main_quit)
        self.window.set_title('Slideshow')
        self.window.set_icon_from_file('icon.svg')
        self.window.modify_bg(gtk.STATE_NORMAL, self.color_bg)
        self.window.add(vbox)
        self.window.show_all()

        self.window.add_events(gtk.gdk.KEY_PRESS_MASK |
              gtk.gdk.POINTER_MOTION_MASK |
              gtk.gdk.BUTTON_PRESS_MASK |
              gtk.gdk.SCROLL_MASK)

        self.window.connect("key-press-event", self.on_key_pressed)


        self.load_file_list()

        self.index = self.get_slideshow_last_index(self.directory)

        if self.FULLSCREEN:
            self.window.fullscreen()

        self.display()
        self.start_tick()


    def on_key_pressed(self, widget, event):
        alt = (event.state & gtk.gdk.MOD1_MASK) == gtk.gdk.MOD1_MASK
        keyname = gtk.gdk.keyval_name(event.keyval)

        if ("Escape" == keyname):
            gtk.mainquit()
        elif ("Right" == keyname):
            if (alt):
                self.on_navigate(10)
            else:
                self.on_navigate(1)
        elif ("Left" == keyname):
            if (alt):
                self.on_navigate(-10)
            else:
                self.on_navigate(-1)
        elif ("space" == keyname):
            if (0 == self.timer):
                print "Pause end"
                self.start_tick()
            else:
                print "Pause start"
                self.stop_tick()
        elif ("r" == keyname or "R" == keyname):
            self.on_navigation_reset()
        else:
            print "pressed %s" % keyname

    def on_navigate(self, nav):
        self.stop_tick()
        self.index += nav
        if (self.display()):
            self.start_tick()

    def on_navigation_reset(self):
        self.stop_tick()
        self.index = 0
        if (self.display()):
            self.start_tick()

    def stop_tick(self):
        if (self.timer > 0):
            glib.source_remove(self.timer)
            self.timer = 0

    def start_tick(self):
        self.timer = glib.timeout_add_seconds(self.SECONDS_BETWEEN_PICTURES, self.on_tick)
        print "start tick %d" % self.timer

    def load_file_list(self):
        """ Find all images """

        # self.frames.append(("l", self.get_slideshow_title(self.directory)))

        foundFiles = []

        for directory, sub_directories, files in os.walk(self.directory):
            for filename in files:
                filepath = os.path.join(directory, filename)

                if (self.is_image(filepath)):
                    self.photosCount += 1
                    foundFiles.append(filepath)

                elif (self.is_movie(filepath)):
                    self.moviesLength += self.get_video_duration(filepath)
                    foundFiles.append(filepath)
                else:
                    print "Nieprawidlowy plik %s" % filepath


        foundFiles.sort()

        self.slides.append((self.SLIDE_LABEL, self.get_slideshow_title(self.directory)))
        
        date = 0

        for filepath in foundFiles:
            filedate = os.path.basename(filepath).split("-")[0]
            
            if (date == 0):
                date = filedate
            elif (date != filedate):
                date = filedate
                self.slides.append((self.SLIDE_LABEL, filedate))
            
            if (self.is_image(filepath)):
                self.slides.append((self.SLIDE_PHOTO, filepath))
            elif (self.is_movie(filepath)):
                self.slides.append((self.SLIDE_VIDEO, filepath))
                
        self.slides.append((self.SLIDE_LABEL, "Koniec"))

        time = (self.photosCount * self.SECONDS_BETWEEN_PICTURES) + int(self.moviesLength)

        print "files=%d, time=%s" % (self.slides.__len__(), self.get_friendly_time_from_seconds(time))

    def display(self):
        self.label.hide()
        self.load_image.hide()

        

        if 0 <= self.index < len(self.slides):
            self.set_slideshow_last_index(self.directory, self.index)

            slideType, content = self.slides[self.index]
            
            print "display index=%d, type=%s, content=%s" % (self.index, slideType, content)

            if (self.SLIDE_PHOTO == slideType):
                print "load image"
                
                #Hide Other
                self.label.hide()
                self.player.stop()
                self.vlc_widget.hide()
                
                #Show image
                self.image.show()
                self.image.set_from_file(content)

            elif (self.SLIDE_VIDEO == slideType):
                print "load movie"
                self.stop_tick()
                
                #Hide Other
                self.label.hide()
                self.image.hide()
                
                #Show vide
                self.vlc_widget.show()
                self.player.set_media(instance.media_new(content))
                self.player.play()

                return False
                
            elif (self.SLIDE_LABEL == slideType):
                print "load label"
                
                #Hide Other
                self.player.stop()
                self.vlc_widget.hide()
                self.image.hide()
                
                #Show label
                self.label.set_text(content)
                self.label.show()
                
                

            return True
        else:
            return False

    def on_video_stop(self, *args, **kwargs):
        print "movie_end_reached"
        self.start_tick()

    def on_tick(self):
        print "tick"
        self.index += 1
        if self.index >= len(self.slides):
            return False

        return self.display()

    @staticmethod
    def get_slideshow_title(directory):
        title_file_path = os.path.join(directory, "title.txt")
        title = "Ladowanie"

        if (os.path.isfile(title_file_path)):
            with open(title_file_path, 'r') as f:
                title = f.readline()

        return title

    @staticmethod
    def get_slideshow_last_index(directory):
        index_file_path = os.path.join(directory, "index.txt")
        index = 0

        if (os.path.isfile(index_file_path)):
            with open(index_file_path, 'r') as f:
                try:
                    index = int(f.readline())
                except:
                    pass

        return index

    @staticmethod
    def set_slideshow_last_index(directory, index):
        index_file_path = os.path.join(directory, "index.txt")

        with open(index_file_path, 'w') as f:
            f.write("%d" % index)

    @staticmethod
    def get_video_duration(file):
        result = subprocess.Popen(["avprobe", "-v", "0", "-show_format", "-of", "json", "%s" % file], stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        str = ''
        for x in result.stdout.readlines():
            str += x
        data = json.loads(str)
        return float(data["format"]["duration"])

    @staticmethod
    def get_friendly_time_from_seconds(time):
        if (time > 3600):
            hours = time / 3600
            minutes = (time % 3600) / 60
            return "%dg %dm" % (hours, minutes)
        elif (time > 60):
            minutes = time / 60
            return "%dm" % minutes

        return "%ds" % time

    @staticmethod
    def is_image(filename):
        """ File is image if it has a common suffix and it is a regular file """
        if not os.path.isfile(filename):
            print "not file"
            return False

        for suffix in ['.jpg', '.png', '.bmp']:
            if filename.lower().endswith(suffix):
                return True

        return False

    @staticmethod
    def is_movie(filename):
        """ File is image if it has a common suffix and it is a regular file """

        if not os.path.isfile(filename):
            return False

        for suffix in ['.mp4', '.avi']:
            if filename.lower().endswith(suffix):
                return True

        return False

if __name__ == "__main__":
    gui = DemoGtk()
    gtk.main()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
